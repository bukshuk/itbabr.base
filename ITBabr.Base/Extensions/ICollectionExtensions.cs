﻿using System;
using System.Collections.Generic;

namespace ITBabr.Base
{
	public static class ICollectionExtensions
    {
        public static void Replace<T>(this ICollection<T> collection, IEnumerable<T> newItems)
        {
            collection.Replace(newItems, (item, _) => item);
        }

        public static void Replace<T1, T2>(this ICollection<T1> collection, IEnumerable<T2> newItems, Func<T2, T1> convert)
        {
            collection.Replace(newItems, (item, _) => convert(item));
        }

        public static void Replace<T1, T2>(this ICollection<T1> collection, IEnumerable<T2> newItems, Func<T2, int, T1> convert)
        {
            collection.Clear();

            if (newItems != null)
            {
                int index = 0;

                foreach (var item in newItems)
                {
                    collection.Add(convert(item, index++));
                }
            }
        }
    }
}
